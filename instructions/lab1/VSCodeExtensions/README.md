# Установка расширений Visual Studio Code

По умолчанию Visual Studio Code идет отображает интерфейс на английском. Вы можете установить установить русский язык, выполнив [эту инструкцию](https://code.visualstudio.com/docs/getstarted/locales).

Большинство инструкций для Visual Studio Code написано на английском. Чтобы проще соотносить интерфейс среды и пункты меню из инструкций, разработчики оставляют язык по умолчанию неизменным.

Об управлении расширениями в Visual Studio Code написано в [статье](https://code.visualstudio.com/docs/editor/extension-marketplace).

Для работы с C++ требуется установить расширение [C/C++ Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools-extension-pack):

![Установка пакетов C/C++ Extension Pack - шаг 1](images/vs-code-install-extensions-step1.png)

![Установка пакетов C/C++ Extension Pack - шаг 2](images/vs-code-install-extensions-step2.png)

![Установка пакетов C/C++ Extension Pack - шаг 3](images/vs-code-install-extensions-step3.png)

Рекомендуется также обратить внимание на расширения:

- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
- [Russian - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-russian)
- [IntelliCode](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)
- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
